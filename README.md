# 开源实习

#### 介绍
线上开源实习项目管理仓库

活动官网 [https://www.openeuler.org/zh/internship/](https://www.openeuler.org/zh/internship/)

#### 参考链接

1. openLooKeng官网：https://openlookeng.io/zh-cn
2. openLooKeng社区博客提交攻略：https://openlookeng.io/zh-cn/blogguidance.html
3. gitee工作流程参考：https://gitee.com/openeuler/community/blob/master/zh/contributors/Gitee-workflow.md
4. openLooKeng社区Bot commands：https://gitee.com/openlookeng/community/blob/master/command.md